package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bean.Book;

public interface BookRepo extends  JpaRepository<Book, Long>{

	List<Book> findByPublished(boolean published);
	
	List<Book> findByTitleContaining(String title);
}
